import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // {
  //   path: '/showview',
  //   name: 'showview',
  //   component: () => import('../views/showView.vue')
  // },
  // {
  //   path: '/table',
  //   name: 'table',
  //   component: () => import('../views/table.vue')
  // },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  // {
  //   path: '/counter',
  //   name: 'counter',
  //   route level code-splitting
  //   this generates a separate chunk (about.[hash].js) for this route
  //   which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/counter.vue')
  // },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users2')
  },
  {
    path: '/products',
    name: 'products',
    component: () => import('../views/Products')
  },
  {
    path: '/customers',
    name: 'customers',
    component: () => import('../views/Customers')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
